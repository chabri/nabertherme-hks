
(function ($, Drupal, debounce) {
  'use strict';

    $.fn.isInViewport = function () {
        if($(this).length){
            let elementTop = $(this).offset().top + 200
            let elementBottom = elementTop + $(this).outerHeight();

            let viewportTop = $(window).scrollTop();
            let viewportBottom = viewportTop + $(window).height();
        return elementBottom > viewportTop && elementTop < viewportBottom;
        }
    };
        
    function barProgress(){
        var itemNews = $('.views-infinite-scroll-content-wrapper .views-row').length,
            element = $('.item-page-bar .bar .loaded'),
            n2 = element.attr("data-total"),
            result = parseFloat(parseInt(itemNews, 10) * 100)/ parseInt(n2, 10);

            element.closest('.item-page-bar').find('.numbers span').text(itemNews);
            element.width(result + '%');
    }
    
    
    $(document).ready(function(){
        
        /* Custom data styles */
        $('[data-bgcolor]').css('background-color', function () {
            return $(this).data('bgcolor')
        });
        
        $('[data-bgoverlay]').each(function(index, el) {
            var $el = $(el);
            var bgcolor = $el.data('bgoverlay');
            if (bgcolor) {
                $el.prepend('<div class="bg-overlay" style="background-color:'+bgcolor+'"></div>');
            }
        });
        $('[data-height]').css('height', function () {
            return $(this).data('height')
        });
        $('[data-margin]').css('margin', function () {
            return $(this).data('margin')
        });
        $('[data-padding]').css('padding', function () {
            return $(this).data('padding')
        });

        $('[data-padding-o]').each(function(index, el) {
            var $el = $(el);
            var src = $el.data('padding-o');
            if (src) {
                $el.parent().css({ padding: src });
                $el.parent().addClass('padding-out');
            }
        });
        $('[data-color]').css('color', function () {
            return $(this).data('color')
        });
        $('[data-text]').css('text-align', function () {
            return $(this).data('text') 
        });
        $('[data-bg]').each(function(index, el) {
            var $el = $(el);
            var src = $el.data('bg');
            if (src) {
                $el.css({ backgroundImage: 'url("' + src + '")' });
            }
        }); 

                
        $('.video-play-button').click(function (e) {      
            e.preventDefault();
            var el = $(this);
            el.addClass('active');
            $(this).closest('.media').find('video').get(0).play();
        });         
        
        $('.open-search, .close-search').click(function (e) {      
            e.preventDefault();
            var el = $(this);
            el.toggleClass('active');
            $('.block-search').toggleClass('active');
        });           

        
        Drupal.behaviors.views_infinite_scroll_automatic = {
            attach : function(context, settings) {
                barProgress();
            },
        };

    });
    

    $(window).on('load', function () {

        barProgress();
        
        $('.js-form-item input').focusin(function(){          
            $(this).parent().addClass('focus');  
        }).focusout(  function(){  
            $(this).parent().removeClass('focus');  
        });
        $('.scroll-to-top a').click( function( e ) {
          e.preventDefault();
          $('html, body').animate( {scrollTop : 0}, 800 );
        });
        $('.video-gallery h3').click(function(e) {
            e.preventDefault();
            let $this = $(this);
            if ($this.hasClass('show')) {
                $this.removeClass('show');
                $this.next().slideUp(350);
                $this.closest('.video-gallery').removeClass('active');
            } else {
                $('.video-gallery').removeClass('active');
                $this.closest('.video-gallery').addClass('active');
                $this.parent().parent().parent().find('.accordion-item').slideUp(350);
                $this.toggleClass('show');
                $this.next().slideToggle();
            }
        });
        
        
        $('.close-lng, .open-lng').on('click', function (e) {      
            e.preventDefault();
            var el = $(this),
                container = el.closest('.block-languageswitcher');
            
            $('body').toggleClass('language-box-active');
        });  

        
        $('body').addClass('loaded');
        $('body').removeClass('loading');
        
        if ($('.section').first().isInViewport()) $('.section').first().addClass('anim');

        if ($('.tab-content h4.mobile').isInViewport()){

            $(this).addClass('fixed')
        }
        $('.tab-content h4.mobile').each(function(i, el){
            
        });
        
        function videoGalleryFunction(data, id){
            var videoContainer = $('#'+id);
            if(data === ''){
                data = 'slide'
                videoCaroucell('', id);
            }
            if(data === 'slide'){
                videoCaroucell('', id);
                videoContainer.removeClass('list-style');
            }else{
                videoContainer.addClass('list-style');
                videoCaroucell('destroy', id);
            }
        }
    
        $('.video-gallery').each(function(index, element){
            $(this).find('.caroucel-video').attr('id', 'video_caroucel_'+index);
            $(this).find('.swiper-pagination').attr('class', 'video_caroucel_pagination_'+index);
            var dataId = $(this).find('.caroucel-video').attr('id'),
                actions = $(this).find('.actions a');
            actions.attr('data-id', dataId);
            videoGalleryFunction('slide', dataId)
            actions.click(function(e){
                e.preventDefault();
                var dataId = $(this).data('id'),
                    data = $(this).data('type');
                $('.actions a').removeClass('active');
                $(this).addClass('active'); 
                videoGalleryFunction(data, dataId)
            });
            
        });

        function videoCaroucell(e, id){
            var slideVideos = new Swiper('#'+id, {
                slidesPerView: 1,
                spaceBetween: 30,
                observer: true,
                observeParents: true,
                breakpoints: {
                    500: {
                    slidesPerView: 2,
                    }
                },
                pagination: {
                    el: '.video_caroucel_pagination_'+id,
                    clickable: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                }
            });
            if (e === 'destroy'){
                slideVideos.destroy(true, true);
                slideVideos = undefined;
                $('#'+id+' .swiper-wrapper, #'+id+' .swiper-slide').attr('style', '');
            }
        }
        $('.header-slide').each(function(index, element){
            $(this).attr('id', 'slide_'+index);
            if ($('#slide_'+index+' .swiper-slide').length > 1) {
                var slide = new Swiper('#slide_'+index, {
                    speed: 600,
                    autoplay: true,
//                    autoHeight: true,
                    pagination: {
                        el: '.c-pag',
                        clickable: true,
                    },
                    navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                    }
                });
            }
        });          

        $('.producte-slide').each(function(index, element){
            $(this).attr('id', 'producte-slide_'+index);
            if ($('#producte-slide_'+index+' .swiper-slide').length > 1) {
                var slide = new Swiper('#producte-slide_'+index, {
                    speed: 600,
                    autoplay: true,
//                    autoHeight: true,
                    pagination: {
                        el: '.c-pag',
                        clickable: true,
                    },
                    navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                    },
                    on: {
                      init: function () {
                            var s = this.activeIndex +1;
                            $('.product-gallery-caption .slide-'+ s).addClass('active');
                      },
                      slideChange: function () {
                            var s = this.activeIndex +1;
                            $('.product-gallery-caption .caption').removeClass('active');
                            $('.product-gallery-caption .slide-'+ s).addClass('active');
                      },
                    },
                });

            }
        });     

        $('.caroucel-slide').each(function(index, element){
            $(this).attr('id', 'caroucel_'+index);
            if ($('#caroucel_'+index+' .swiper-slide').length > 1) {
                var slide = new Swiper('#caroucel_'+index, {
                    slidesPerView: 1,
//                    centeredSlides: true,
                    breakpoints: {
                        500: {
                        slidesPerView: 2,
                        },
                        768: {
                        slidesPerView: 3,
                        },
                        1024: {
                        slidesPerView: 4,
                        }
                    },
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    }
                });
            }
        });   

        $('.paragraph--type--reference-block').each(function(){
            var element = $(this).find('.caroucel-slide');
            if(element.length){
                $(this).addClass('caroucel-container');
            }
           
        });
        $('.tabs-produkte').each(function(){
             var tabs = [],
                 $container = $('.tabs-slide-content'),
                 $tabTileClass = $container.find('.tab-item');
                 $tabTileClass.first().parent().addClass('first');
             if ($tabTileClass.length > 1) {
                $tabTileClass.each(function() {
                    var $tabTile = $(this);
                    var text = $tabTile.data('title');
                    var id = $tabTile.data('id');
                    tabs.push('<li class="swiper-slide" data-id="'+id+'"><span>' + text + '</span></li>');
                });
                var $tabsHTML = $('<div id="tabs-menu" class="swiper-container tabs-slide"><ul class="tabs swiper-wrapper">' + tabs.join('') + '</ul></div>').clone(false);
                $container.before($tabsHTML);
                $('#tabs-menu li').first().addClass('active');
             }
        });

        $('.tabs-slide-content').each(function(index, element){
            $(this).attr('id', 'tabslide_'+index);
        });
        
        var slideTabs = new Swiper('.tabs-slide', {
            slidesPerView: 'auto',
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
//            simulateTouch: false,
            allowTouchMove: false,
        });
        var slideTabsContent = new Swiper('.tabs-slide-content', {
            speed: 600,
            simulateTouch: false,
            allowTouchMove: false,
            autoHeight: true,
            thumbs: {
                swiper: slideTabs
            }
        });
        $('.go-to-tab').click(slideTabsContent,function(){
            slideTabsContent.slideTo($(this).data('slide'));
        })
        /* Pull menu */
        $('.pull-menu').click(function(){ 
            $('body').toggleClass('pull-active');            
            $('.pull-menu').toggleClass('active');                 
            $('.overlay-menu').toggleClass('active');                 
        });
        $('.overlay-menu').click(function(){           
            $('body').removeClass('pull-active');      
            $(this).removeClass('active');    
            $('.pull-menu').removeClass('active');     
        });
        
       
        $(window).on("resize", function () {
            var width = $(window).width();
            $(".paragraph--type--text-media").each(function(){
                var paragraph = $(this),
                c = $(".paragraph--type--text-media .container").offset().left;
                if(paragraph.hasClass('out-of-container')){
                    if(paragraph.hasClass('col-33-67')){
                        if(paragraph.hasClass('content-left')){
                            paragraph.find('.media').css({
                                "width": "calc(67% + "+c+"px)",
                                "right": "-"+c+"px"
                            });
                        }else{
                            paragraph.find('.media').css({
                                "width": "calc(67% + "+c+"px)",
                                "left": "-"+c+"px"
                            }); 
                        }
                    }            
                    if(paragraph.hasClass('col-67-33')){
                        if(paragraph.hasClass('content-left')){
                            paragraph.find('.media').css({
                                "width": "calc(33% + "+c+"px)",
                                "right": "-"+c+"px"
                            });
                        }else{
                            paragraph.find('.media').css({
                                "width": "calc(33% + "+c+"px)",
                                "left": "-"+c+"px"
                            }); 
                        }
                    }
                }
            });
            $('#tabs-menu li').on('click', function(){
                var id = $(this).data('id');
                if(width <= 600){
                    $("html, body").animate({ scrollTop: $('#' + id).offset().top -10 }, 500);    
                }else{
                    $("html, body").animate().stop();    
                }
            });
        }).resize();
    });
    
    var headerHeight = $('#header').outerHeight();
    $('body:not(.toolbar-fixed) #main').css('padding-top', headerHeight);

    var c, 
        currentScrollTop = 0,
        navbar = $('#header');
    
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        $('.section').each(function(i, el){
            if ($(this).isInViewport()) $(this).addClass('anim');
        });
        $('.rows-item .views-row').each(function(i, el){
            if ($(this).isInViewport()) $(this).addClass('anim');
        });

        var a = $(window).scrollTop();
        var b = navbar.outerHeight();
        currentScrollTop = a;
        if (c < currentScrollTop && a > b + b) {
            navbar.css("scrollUp");
            navbar.css('top', - b);
        } else if (c > currentScrollTop && !(a <= b)) {
            navbar.css('top', 0);
        } 
        if (currentScrollTop >= b) {
            navbar.addClass('active');
        }else{
            navbar.removeClass('active');
        }
        c = currentScrollTop;
    });
    
})(jQuery, Drupal, Drupal.debounce);

