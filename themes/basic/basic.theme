<?php

/**
 * @file
 * Preprocess functions for Basic.
 */

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Language\LanguageInterface;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\field\Entity\FieldConfig;
use \Drupal\taxonomy\Entity\Term;
use Drupal\image\Entity\ImageStyle;
/**
 * Implements hook_page_attachments_alter().
 */
function basic_css_alter(&$css, \Drupal\Core\Asset\AttachedAssetsInterface $assets) {

  // Remove defaults.css file.
  unset($css[drupal_get_path('module', 'we_megamenu') . '/assets/css/we_megamenu_backend.css']);
  unset($css[drupal_get_path('module', 'we_megamenu') . '/assets/includes/bootstrap/css/bootstrap.min.css']);

}

function basic_preprocess(&$variables, $hook){
    $variables['base_path'] = \Drupal::request()->getSchemeAndHttpHost();
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $variables['language'] = $language;
}


function basic_preprocess_html(&$variables) {
  try {
    $variables['is_front'] = \Drupal::service('path.matcher')->isFrontPage();
  }
  catch (Exception $e) {
    $variables['is_front'] = FALSE;
  }

  if (!$variables['is_front']) {
    $path = \Drupal::service('path.current')->getPath();
    
    $variables['classes_array'][] = 'front-page';
    $alias = \Drupal::service('path_alias.manager')->getAliasByPath($path);
    $alias = trim($alias, '/');
    if (!empty($alias)) {
      $name = str_replace('/', '-', $alias);
      $variables['attributes']['class'][] = 'page-' . $name;
      list($section,) = explode('/', $alias, 2);
      if (!empty($section)) {
        $variables['attributes']['class'][] = 'section-' . $section;
      }
    }
  }else{
      $variables['attributes']['class'][] = 'front-page';
  }

  $theme_name = \Drupal::theme()->getActiveTheme()->getName();
  $theme_settings = \Drupal::config($theme_name . '.settings');
  CacheableMetadata::createFromRenderArray($variables)
    ->addCacheableDependency($theme_settings)
    ->applyTo($variables);
  $variables += $theme_settings->getOriginal();
}

function basic_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    $content_type = $node->bundle();
    $suggestions[] = 'page__'.$content_type;
  }
}


function basic_preprocess_paragraph(&$variables) {
    $content = $variables['content'];
    $paragraph_target = $variables['paragraph']->getParagraphType()->id;
    $story_attributes = isset($variables['attributes']) ? $variables['attributes'] : array();
    $classes = isset($variables['attributes']['class']) ? $variables['attributes']['class'] : array();  
    
    if (isset($content['field_padding'][0])) {
        $story_attributes['data-padding'] = $content['field_padding'][0]['#markup'];
        hide($variables['content']['field_padding']);
    }   
    if (isset($content['field_margin'][0])) {
        $story_attributes['data-margin'] = $content['field_margin'][0]['#markup'];
        hide($variables['content']['field_margin']);
    }
    
    if (isset($content['field_background_color'][0]['#markup'])) {
        $story_attributes['data-bgcolor'] = $content['field_background_color'][0]['#markup'];
        hide($variables['content']['field_background_color']);
    }    

    if (isset($content['field_background_overlay'][0]['#markup'])) {
        $story_attributes['data-bgoverlay'] = $content['field_background_overlay'][0]['#markup'];
        hide($variables['content']['field_background_overlay']);
    }
    if (isset($content['field_slide_image']['#items'])) {
        $story_attributes['data-bgimagedesk'] = $content['field_slide_image'][0]['#markup'];
        hide($variables['content']['field_slide_image']);
    }
    if (isset($content['field_background_image']['#items'])) {
        $story_attributes['data-bg'] = $content['field_background_image'][0]['#markup'];
        hide($variables['content']['field_background_image']);
    }
    if (isset($content['field_background_image_med']['#items'])) {
        $story_attributes['data-bg'] = $content['field_background_image_med'][0]['#markup'];
        hide($variables['content']['field_background_image_med']);
    }
    

    if (isset($content['field_content_align_horizontal'])) {
        hide($variables['content']['field_content_align_horizontal']);
    }

    if (isset($content['field_inverted_row'])) {
        hide($variables['content']['field_inverted_row']);
    }
    
    if (isset($content['field_out_of_container']['#items'])) {
        if($content['field_out_of_container']['#items']->value == '1'){
            $classes[] = 'out-of-container';    
        }
        hide($variables['content']['field_out_of_container']);
    }    
    if (isset($content['field_line_separator']['#items'])) {
        if($content['field_line_separator']['#items']->value == '1'){
            $classes[] = 'separator-line';    
        }
        hide($variables['content']['field_line_separator']);
    }

    
    if (isset($content['field_stick_to_the_side']['#items'])) {
        $classes[] = $content['field_stick_to_the_side']['#items']->value;    
        hide($variables['content']['field_stick_to_the_side']);
    }
    if (isset($content['field_column_size']['#items'])) {
        $classes[] = $content['field_column_size']['#items']->value;    
        hide($variables['content']['field_column_size']);
    }
    if (isset($content['field_content_align'])) {
        $classes[] = $content['field_content_align']['#items']->value;    
        hide($variables['content']['field_content_align']);
    }
    if (isset($content['field_color'][0]['#markup'])) {
        $classes[] = $content['field_color']['#items']->value;
        hide($variables['content']['field_color']);
    }
    if (isset($content['field_form_style'][0]['#markup'])) {
        $classes[] = $content['field_form_style']['#items']->value;
        hide($variables['content']['field_form_style']);
    }
    /* Clases */
    $columnClass = "";
    $containerClass = 'container';
    
    if (isset($content['field_remove_container']['#items'])) {
        if($content['field_remove_container']['#items']->value == '1'){
            $containerClass = "fluid-container";
        }
        hide($variables['content']['field_remove_container']);
    }
    
    if (isset($content['field_column'])) {
        $columnClass = $content['field_column']['#items']->value;
        hide($variables['content']['field_column']);
    }      
    if (isset($content['field_container']['#items'])) {
        $containerClass = $content['field_container']['#items']->value;
        hide($variables['content']['field_container']);
    }  

    $variables['columnClass'] = $columnClass;
    $variables['containerClass'] = $containerClass;
    $variables['attributes'] = $story_attributes;
    $variables['attributes']['class'] = $classes;

}
function basic_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  // Block suggestions for custom block bundles.
  if (isset($variables['elements']['content']['#block_content'])) {
    array_splice($suggestions, 1, 0, 'block__bundle__' . $variables['elements']['content']['#block_content']->bundle());
  }
}
function term_depth($tid) {
    $tid = Drupal::database()->query(
    "SELECT parent_target_id FROM {taxonomy_term__parent} WHERE entity_id = :tid",
    [':tid' => $tid]
    )->fetchField();
    
    if ($tid == 0) {
        return 1;
    }
    else {
        return 1 + term_depth($tid);
    }
}


function basic_preprocess_node(&$variables) {
    $classes = isset($variables['attributes']['class']) ? $variables['attributes']['class'] : array();  
    $content = $variables['content'];
  
    if (isset($content['field_container']['#items'])) {
         hide($content['field_container']);
        $classes[] = $content['field_container']['#items']->value;
       
    }  
    $variables['attributes']['class'] = $classes;
}

/* TEMPORAL */
function basic_preprocess_layout(&$variables) {
    $languagecode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $default_languagecode = \Drupal::languageManager()->getDefaultLanguage()->getId();
    $languagePrefix = ($languagecode == $default_languagecode) ? '' : '/'.$languagecode;
        
    $aliasManager = \Drupal::service('path_alias.manager');
    
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if(!empty($variables['content']['#node'])){
        
        $nodeType = $variables['content']['#node']->getType();

        if($nodeType === 'produkte'){
            $tid = $variables['content']['#node']->get('field_produktkategorie')->target_id;
            $parents = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($tid);
            if(!empty($parents)){
                $parentKatalogTermn = reset($parents)->id();
            }else{
                $parentKatalogTermn = $tid;
            }
            
            $variables['parentTaxonomie'] = $parentKatalogTermn;
        }
    }
    
    if(\Drupal::routeMatch()->getRawParameter('taxonomy_term')){
        if(!empty($variables['content']['#taxonomy_term'])){
            $tid = \Drupal::routeMatch()->getRawParameter('taxonomy_term');
            $childrens = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($tid);
            $parent = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($tid);
            $parent = reset($parent);
            $list = [];
            $media_url = '';
            $variables['HasNotMultipleChildrens'] = '';
            if(!empty($parent)){
                foreach (\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($parent->id()) as $term) {
                    if(isset($alias)){
                        $alias = $languagePrefix.\Drupal::service('path_alias.manager')->getAliasByPath('/taxonomy/term/'.$term->id());
                    }else{
                        $alias = '/'.$language . $term->get('path')->alias;
                    }
                    if($tid != $term->id()){
                        
                        if(!empty($term->get("field_kategorie_images"))){
                            $media_entity_load = Media::load($term->get("field_kategorie_images")->getString());
                             if(!empty($media_entity_load)){
                            $uri = $media_entity_load->field_media_image->entity->getFileUri();
                            $media_url = ImageStyle::load('product_category_thumbnail')->buildUrl($uri);
                             }
                        }
                    $list[] = array('name' => $term->label(), 'image' => $media_url, 'url' => $alias);
                    }
                }

            }
           
            if($parent === false){
                $load_entities = FALSE;
                $c = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($tid);
                $firstKey = array_shift($c);
                $cd = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($firstKey->id());
                if(empty($cd)){
                    $variables['HasNotMultipleChildrens'] = 'false';
                }
            }
            
             $variables['relatedChildrensItems'] = $list;
            if(!empty($childrens)){
                $variables['childrens'] = 'true';
            }
        }
    }
}
function basic_theme_suggestions_field_alter(&$suggestions, $variables) {
    $suggestions[] = 'field__' . 
    $variables['element']['#field_name'] . '__' . 
    $variables['element']['#view_mode'];
}


function basic_theme_suggestions_container_alter(&$suggestions, array $variables) {
  $element = $variables['element'];

  if (isset($element['#type']) && $element['#type'] == 'view') {
    $suggestions[] = 'container__' . $element['#name'];
    $suggestions[] = 'container__' . $element['#name'] . '__' . $element['#display_id'];
  }

  if (isset($element['#type']) && $element['#type'] == 'container' && isset($element['children']['#type'])) {
    $suggestions[] = 'container__' . $element['children']['#type'];
  }
}