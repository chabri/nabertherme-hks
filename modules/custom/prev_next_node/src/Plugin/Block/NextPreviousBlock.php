<?php

/**
 * @file
 * Contains \Drupal\prev_next_node\Plugin\Block\NextPreviousBlock.
 */

namespace Drupal\prev_next_node\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a 'Next Previous' block.
 *
 * @Block(
 *   id = "next_previous_block",
 *   admin_label = @Translation("Next Previous Block"),
 *   category = @Translation("Blocks")
 * )
 */
class NextPreviousBlock extends BlockBase {
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * {@inheritdoc}
   */
  public function build() {
    $link = [];

    // Get the created time of the current node.
    $node = \Drupal::routeMatch()->getParameter('node');



    if ($node instanceof NodeInterface && $node->getType() == 'aktuelles') {
      $current_nid = $node->id();

      $prev = $this->generatePrevious($node);
      if (!empty($prev)) {
        $link['prev'] = $prev;
      }

      $next = $this->generateNext($node);
      if (!empty($next)) {
        $link['next'] = $next;
      }
    }
    return $link;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // Get the created time of the current node.
    $node = \Drupal::routeMatch()->getParameter('node');


//    $node = $this->routeMatch->getParameter('node');
    if (!empty($node) && $node instanceof NodeInterface) {
      // If there is node add its cachetag.
      return Cache::mergeTags(parent::getCacheTags(), ['node:*']);
    }
    else {
      // Return default tags instead.
      return parent::getCacheTags();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

  /**
   * Lookup the previous node,youngest node which is still older than the node.
   *
   * @param string $current_nid
   *   Show current page node id.
   *
   * @return array
   *   A render array for a previous node.
   */
  private function generatePrevious($node) {
    return $this->generateNextPrevious($node, 'prev');
  }

  /**
   * Lookup the next node,oldest node which is still younger than the node.
   *
   * @param string $current_nid
   *   Show current page node id.
   *
   * @return array
   *   A render array for a next node.
   */
  private function generateNext($node) {
    return $this->generateNextPrevious($node, 'next');
  }

  const DIRECTION__NEXT = 'next';

  /**
   * Lookup the next or previous node.
   *
   * @param string $current_nid
   *   Get current page node id.
   * @param string $direction
   *   Default value is "next" and other value come from
   *   generatePrevious() and generatePrevious().
   *
   * @return array
   *   Find the alias of the next node.
   */
    
    
  private function generateNextPrevious($node, $direction = self::DIRECTION__NEXT) {
    $comparison_opperator = '>';
    $sort = 'ASC';
    $class = 'btn btn-next';
    $current_nid = $node->id();
    $current_langcode = $node->get('langcode')->value;

    if ($direction === 'prev') {
      $comparison_opperator = '<';
      $sort = 'DESC';
      $class = 'btn btn-prev';
    }

    // Lookup 1 node younger (or older) than the current node.
     $query = \Drupal::entityQuery('node');
//    $query_result = $query->getQuery();
     $next = $query->condition('nid', $current_nid, $comparison_opperator)
      ->condition('type', 'aktuelles')
      ->condition('status', 1)
      ->condition('langcode', $current_langcode)
      ->sort('nid', $sort)
      ->range(0, 1)
      ->execute();
    if (!empty($next) && is_array($next)) {
      $next = array_values($next);
      $next = $next[0];
      $load_node = \Drupal\node\Entity\Node::load($next);
      $display_text = $load_node->getTitle();
      // Find the alias of the next node.
      $nid = $next;
      $url = Url::fromRoute('entity.node.canonical', ['node' => $nid], []);
      $link = Link::fromTextAndUrl($display_text, Url::fromUri('internal:/' . $url->getInternalPath()));
      $link = $link->toRenderable();
      $link['#attributes'] = ['class' => ['nextpre__btn', $class]];
      return $link;
    }
    return '';
  }
}
