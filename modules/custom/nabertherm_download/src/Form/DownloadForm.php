<?php

namespace Drupal\nabertherm_download\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\nabertherm_download\GuardInterface;
use Drupal\nabertherm_download\DownloadHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Download Form.
 */
class DownloadForm extends FormBase {

  /**
   * Access guard implementation.
   *
   * @var \Drupal\nabertherm_download\GuardInterface
   */
  protected $guard;

  /**
   * The download helper service.
   *
   * @var \Drupal\nabertherm_download\DownloadHelper
   */
  protected $downloadHelper;

  /**
   * Creates a new instance of the DownloadForm class.
   *
   * @param \Drupal\nabertherm_download\GuardInterface $guard
   *   The access guard for download files.
   * @param \Drupal\nabertherm_download\DownloadHelper $download_helper
   *   The download helper service.
   */
  public function __construct(GuardInterface $guard, DownloadHelper $download_helper) {
    $this->guard = $guard;
    $this->downloadHelper = $download_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('nabertherm_download.guard'),
      $container->get('nabertherm_download.download_helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nabertherm_download_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $route_name = NULL) {
    $form['type'] = [
      '#type' => 'hidden',
      '#value' => 'pdf',
    ];

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product', [], ['context' => 'Download Form']),
      '#required' => TRUE,
    ];

    $form['token'] = [
      '#type' => 'password',
      '#title' => $this->t('Password', [], ['context' => 'Download Form']),
    ];

    $form['actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-actions'],
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Download', [], ['context' => 'Download Form']),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $name = $this->downloadHelper->slugify($form_state->getValue('name'));
    $type = $form_state->getValue('type');
    $token = $form_state->getValue('token');

    $uri = $this->downloadHelper->getFileUri($name . '.' . $type);

    // Ensure that only supported file types are requested.
    if (!in_array($type, ['pdf'])) {
      $form_state->setErrorByName('type', $this->t('Invalid file type.'));
    }

    // Ensure that a valid product name is given.
    if (empty($name)) {
      $form_state->setErrorByName('name', $this->t('The product name is required.'));
    }

    // Ensure that a download file exists for the given product.
    if (!is_readable($uri)) {
      $form_state->setErrorByName('name', $this->t('No download file found for the given product.'));
    }

    // Ensure that the user provided a valid access token.
    if (empty($token) && $this->guard->isProtected($uri)) {
      $form_state->setErrorByName('name', $this->t('A password is required to download the given file.'));
    }
    elseif (!$this->guard->canDownload($uri, $token)) {
      $form_state->setErrorByName('name', $this->t('Invalid password.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $name = $this->downloadHelper->slugify($form_state->getValue('name'));
    $type = $form_state->getValue('type');
    $token = $form_state->getValue('token');

    $uri = $this->downloadHelper->getFileUri($name . '.' . $type);

    // Ensure that the given download file exists.
    if (!is_readable($uri)) {
      throw new NotFoundHttpException();
    }

    // Ensure that the given download file is accessible by the current user.
    if (!$this->guard->canDownload($uri, $token)) {
      throw new AccessDeniedHttpException();
    }

    $form_state->setResponse(
      new BinaryFileResponse($uri, 200, $this->downloadHelper->getFileHeaders($uri), FALSE)
    );
  }

}
