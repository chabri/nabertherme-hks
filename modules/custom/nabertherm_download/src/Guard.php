<?php

namespace Drupal\nabertherm_download;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\File\FileSystemInterface;

/**
 * Check access to a protected download files.
 */
class Guard implements GuardInterface {

  /**
   * The filesystem implementation.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * File extension used for access tokens.
   *
   * @var string
   */
  protected $extension;

  /**
   * Create a new instance of the download guard.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system implementation.
   * @param string $extension
   *   The file extension to use for files holding the access token.
   */
  public function __construct(FileSystemInterface $file_system, $extension = 'passwd.txt') {
    $this->fileSystem = $file_system;
    $this->extension = $extension;
  }

  /**
   * {@inheritDoc}
   */
  public function isProtected($uri) {
    return is_readable($this->getTokenPath($uri));
  }

  /**
   * Get the path of the file holding the access token.
   *
   * @param string $uri
   *   The file uri.
   *
   * @return string
   *   The path of the file holding the access token.
   */
  public function getTokenPath($uri) {
    $extension = pathinfo($uri, PATHINFO_EXTENSION);

    $basename = $this->fileSystem->basename($uri, ".${extension}");
    $dirname = $this->fileSystem->dirname($uri);

    return $dirname . DIRECTORY_SEPARATOR . $basename . '.' . $this->extension;
  }

  /**
   * Load the required access token for a given file.
   *
   * @param string $uri
   *   The file uri.
   *
   * @return string
   *   The access token for the given file download.
   */
  public function getToken($uri) {
    return rtrim(file_get_contents($this->getTokenPath($uri)));
  }

  /**
   * {@inheritDoc}
   */
  public function canDownload($uri, $token = NULL) {
    $scheme = StreamWrapperManager::getScheme($uri);

    // Return early if the file is not within the download folder.
    if ($scheme !== 'download') {
      return FALSE;
    }

    // Return early if the given file does not exist or is not readable.
    if (!is_readable($uri)) {
      return FALSE;
    }

    // Ensure that the given file requires an access token.
    if (!$this->isProtected($uri)) {
      return TRUE;
    }

    // Check whether a valid access token is provided.
    return isset($token) && $token === $this->getToken($uri);
  }

  /**
   * {@inheritDoc}
   */
  public function access($uri, $token = NULL) {
    if (!$this->canDownload($uri, $token = NULL)) {
      return AccessResult::forbidden();
    }

    return AccessResult::neutral();
  }

}
