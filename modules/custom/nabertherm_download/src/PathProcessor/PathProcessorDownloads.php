<?php

namespace Drupal\nabertherm_download\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a path processor to rewrite file URLs.
 *
 * As the route system does not allow arbitrary amount of parameters convert
 * the file path to a query parameter on the request.
 *
 * @see Drupal\system\PathProcessor\PathProcessorFiles::processInbound()
 */
class PathProcessorDownloads implements InboundPathProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    if (strpos($path, '/nabertherm/downloads/') === 0 && !$request->query->has('file')) {
      $request->query->set('file', preg_replace('|^\/nabertherm\/downloads\/|', '', $path));
      return '/nabertherm/downloads';
    }
    return $path;
  }

}
