<?php

namespace Drupal\nabertherm_download\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\nabertherm_download\GuardInterface;
use Drupal\nabertherm_download\DownloadHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * File download controller.
 */
class DownloadController extends ControllerBase {

  /**
   * The access guard implementation.
   *
   * @var \Drupal\nabertherm_download\GuardInterface
   */
  protected $guard;

  /**
   * The download helper service.
   *
   * @var \Drupal\nabertherm_download\DownloadHelper
   */
  protected $downloadHelper;

  /**
   * Creates a new instance of the FileDownloadController class.
   *
   * @param \Drupal\nabertherm_download\GuardInterface $guard
   *   The access guard implementation.
   * @param \Drupal\nabertherm_download\DownloadHelper $download_helper
   *   The download helper service.
   */
  public function __construct(GuardInterface $guard, DownloadHelper $download_helper) {
    $this->guard = $guard;
    $this->downloadHelper = $download_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('nabertherm_download.guard'),
      $container->get('nabertherm_download.download_helper')
    );
  }

  /**
   * Handle file downloads.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   The transferred file as response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown when the requested file does not exist.
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when the user does not have access to the file.
   */
  public function download(Request $request) {
    $file = $request->get('file');
    $token = $request->get('token');

    $uri = 'download://' . $file;

    if (!is_file($uri)) {
      throw new NotFoundHttpException();
    }

    if (!$this->guard->canDownload($uri, $token)) {
      throw new AccessDeniedHttpException();
    }

    return new BinaryFileResponse($uri, 200, $this->downloadHelper->getFileHeaders($uri), FALSE);
  }

}
