<?php

namespace Drupal\nabertherm_download;

/**
 * Provides an interface for services that manage access to protected files.
 */
interface GuardInterface {

  /**
   * Determine whether the given file requires an access token.
   *
   * @param string $uri
   *   The file uri.
   *
   * @return bool
   *   True if access to the given file is restricted.
   */
  public function isProtected($uri);

  /**
   * Determine whether the given file can be downloaded.
   *
   * @param string $uri
   *   The uri of the file to download.
   * @param string $token
   *   (Optional) The access token if provided.
   *
   * @return bool
   *   TRUE if the file can be downloaded, FALSE otherwise.
   */
  public function canDownload($uri, $token = NULL);

  /**
   * Determine whether a user has access to a given download file.
   *
   * @param string $uri
   *   The uri of the file to download.
   * @param string $token
   *   (Optional) The access token.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access($uri, $token = NULL);

}
