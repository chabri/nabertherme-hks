<?php

namespace Drupal\nabertherm_download;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\File\FileSystemInterface;

/**
 * Helper service that can be used to generate the path of download files.
 */
class DownloadHelper {

  /**
   * The filesystem implementation.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Create a new instance of the Download Helper class.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system implementation.
   */
  public function __construct(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
  }

  /**
   * Get HTTP response headers for a given download file.
   *
   * @param string $uri
   *   The uri of the file.
   *
   * @return array
   *   An array of HTTP headers.
   */
  public function getFileHeaders($uri) {
    $name = Unicode::mimeHeaderEncode($this->fileSystem->basename($uri));
    $mime = Unicode::mimeHeaderEncode(mime_content_type($uri));
    $size = Unicode::mimeHeaderEncode(filesize($uri));

    return [
      'Content-Transfer-Encoding' => 'binary',
      'Content-Disposition'       => 'attachment; filename="' . $name . '"',
      'Content-Type'              => $mime,
      'Content-Length'            => $size,
      'Pragma'                    => 'no-cache',
      'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
      'Expires'                   => '0',
    ];
  }

  /**
   * Get path information for the given download file.
   *
   * @param string $file
   *   The file to retrieve path information for.
   *
   * @return array
   *   Returns an array with path information.
   */
  public function getPathInfo($file) {
    $extension = pathinfo($file, PATHINFO_EXTENSION);

    $dirname = $this->fileSystem->dirname($file);
    $basename = empty($extension)
      ? $this->fileSystem->basename($file)
      : $this->fileSystem->basename($file, ".${extension}");

    return [
      'dirname' => $dirname,
      'basename' => $basename,
      'extension' => $extension,
    ];
  }

  /**
   * Generate the URI for a given download file.
   *
   * @param string $file
   *   The name of the file (including the extension).
   *
   * @return string
   *   Returns the file uri.
   */
  public function getFileUri($file) {
    return 'download://' . $this->getFilePath($file);
  }

  /**
   * Generate the path for a given download file.
   *
   * @param string $file
   *   The name of the file (including the extension).
   *
   * @return string
   *   Return the file path.
   */
  public function getFilePath($file) {
    $path_info = $this->getPathInfo($file);

    $path = '';

    if (!empty($path_info['dirname'])) {
      $path .= $path_info['dirname'] . DIRECTORY_SEPARATOR;
    }

    if (!empty($path_info['basename'])) {
      $path .= $path_info['basename'] . DIRECTORY_SEPARATOR . $path_info['basename'];
    }

    if (!empty($path_info['extension'])) {
      $path .= '.' . $path_info['extension'];
    }

    return rtrim(ltrim($path, './'), '/');
  }

  /**
   * Makes a string safe to use in a URL.
   *
   * @param string $string
   *   The string to modify.
   * @param string $separator
   *   The character to use as a replacement.
   *
   * @return string
   *   Returns a string that is safe to be used in a URL.
   */
  public function slugify($string, $separator = '-') {
    // Convert to lowercase.
    $string = strtolower($string);

    // Replace german umlauts.
    $string = str_replace(['ä', 'ö', 'ü', 'ß'], ['ae', 'oe', 'ue', 'ss'], $string);

    // Replace non alphanumeric characters.
    $string = preg_replace('/[^a-z0-9]/i', $separator, $string);

    // Remove multiple occurances of the separator.
    $string = preg_replace('/' . preg_quote($separator) . '[' . preg_quote($separator) . ']*/', $separator, $string);

    // Remove the separator from the start and end of the string.
    $string = preg_replace('/' . preg_quote($separator) . '$/', '', $string);
    $string = preg_replace('/^' . preg_quote($separator) . '/', '', $string);

    return $string;
  }

}
