<?php

namespace Drupal\nabertherm_download\StreamWrapper;

use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\LocalStream;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Nabtertherm download (download://) stream wrapper class.
 *
 * Provides support for storing password protected download files with the
 * Drupal file interface.
 */
class DownloadStream extends LocalStream {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getType() {
    return StreamWrapperInterface::LOCAL_NORMAL;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->t('Download files');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Protected downloads served by Drupal.');
  }

  /**
   * {@inheritdoc}
   */
  public function getDirectoryPath() {
    return Settings::get('nabertherm_download_path');
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());

    $route = Url::fromRoute('nabertherm_download.protected_file_download', [
      'filepath' => $path,
      'scheme' => 'download',
    ], [
      'absolute' => TRUE,
    ]);

    return $route->toString();
  }

}
