<?php

namespace Drupal\nabertherm_produkt_importer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\media\Entity\Media;

/**
 * Class ImportController.
 */
class ImportController extends ControllerBase
{

  /**
   * Import.
   *
   * @return string
   *   Return Hello string.
   */
  public function import() {
    $messages = [];
    $map = scandir(DRUPAL_ROOT . '/import');

    // $storage_handler = \Drupal::entityTypeManager()->getStorage("node");
    // $entities = $storage_handler->loadByProperties(["type" => "produkte"]);
    // $storage_handler->delete($entities);

    foreach ($map as $folder) {
      if (substr($folder, 0, 1) == '.') {
        continue;
      }

      $import_dir = scandir(DRUPAL_ROOT . '/import/' . $folder);
      foreach ($import_dir as $import_folder) {
        if (substr($import_folder, 0, 1) == '.') {
          continue;
        }

        $products_file = $this->_convert(@file_get_contents(DRUPAL_ROOT . '/import/' . $folder . '/' . $import_folder . '/' . $folder . '_' . $import_folder . '.xml'), 'XML');
        $products = @simplexml_load_string($products_file);

        if ($products !== FALSE) {
          $messages[] = 'Importing: '. $folder . '/' . $import_folder . '/' . $folder . '_' . $import_folder . '.xml';

          foreach ($products->produkt_gesamt as $produkt) {
            $prod = $produkt->deutsch;

            /**
             * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
             *   ANPASSEN
             * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
             */
            $produktbereich = 2;

            $title = $this->_import_ueberschrift($prod->ueberschrift) ?? 'Unbekanntes Produkt';
            $body = $this->_import_fliesstext_gesamt($prod->fliesstext_gesamt) . $this->_processListen($prod->aufzaehlung_gesamt, 'deutsch', false);
            $listen = $this->_processListen($prod->aufzaehlung_gesamt, 'deutsch', true);
            $tabelle = $this->_import_tabelle_gesamt($prod->tabelle_gesamt);
            $images = $this->_processImages($produkt, DRUPAL_ROOT . '/import/' . $folder . '/' . $import_folder);
            $zusaetliche_seiten = '';
            $zus_pages = ['zweiteseite', 'dritteseite', 'vierteseite', 'fuenfteseite'];
            foreach ($zus_pages as $page_name) {
              $page_name = 'text_' . $page_name;
              $zusaetliche_seiten .= $this->_import_zusaetz_seiten($prod->$page_name);
            }

            $nids = \Drupal::entityQuery('node')
              ->condition('title', $title)
              ->condition('type', 'produkte')
              ->execute();

            if (count($nids)) {
              $nid = array_shift($nids);
              $node = \Drupal\node\Entity\Node::load($nid);
            }
            else {
              $node = Node::create(['type' => 'produkte']);
              $node->promote = 0;
              $node->sticky = 0;
            }

            $node->title = $title;
            $node->body->setValue(['value' => $body, 'format' => 'full_html']);
            $node->field_additional_equipment->setValue(['value' => $listen, 'format' => 'full_html']);
            $node->field_tabelle->setValue(['value' => $tabelle, 'format' => 'full_html']);
            $node->field_zusatzliche_seiten->setValue(['value' => $zusaetliche_seiten, 'format' => 'full_html']);
            $node->field_produktkategorie = [['target_id' => $produktbereich]];

            $produkt_images = [];
            foreach($images as $image => $caption) {
              $produkt_images[] = ['target_id' => $image];
            }
            if (count($images)) {
              $node->field_produkte_image = $produkt_images;
            }

            $node->save();

            $nid = $node->id(); // Get Nid from the node object.

            // Node translation
            $translations = array(
              'test' => 'test',
              'en' => 'englisch',
              'es' => 'spanisch',
              'fr' => 'franzoesisch',
              'it' => 'italienisch',
              'pl' => 'polnisch',
              'ru' => 'russisch',
              'ja' => 'japanisch',
              'zh-hans' => 'chinesisch',
            );
            foreach ($translations as $key => $value) {
              if (!isset($produkt->$value)) {
                continue;
              }
              $prod = $produkt->$value;

              $title = $this->_import_ueberschrift($prod->ueberschrift) ?? 'Unbekanntes Produkt';
              $body = $this->_import_fliesstext_gesamt($prod->fliesstext_gesamt) . $this->_processListen($prod->aufzaehlung_gesamt, $value, false);
              $listen = $this->_processListen($prod->aufzaehlung_gesamt, $value, true);
              $tabelle = $this->_import_tabelle_gesamt($prod->tabelle_gesamt);

              $zus_pages = ['zweiteseite', 'dritteseite', 'vierteseite', 'fuenfteseite'];
              foreach ($zus_pages as $page_name) {
                $page_name = 'text_' . $page_name;
                $zusaetliche_seiten .= $this->_import_zusaetz_seiten($prod->$page_name);
              }

              $transNode = Node::load($nid);
              if (!$transNode->hasTranslation($key)) {
                $transNode = $transNode->addTranslation($key);
                $transNode->title = $title;
                $transNode->body->setValue(['value' => $body, 'format' => 'full_html']);
                $transNode->field_additional_equipment->setValue(['value' => $listen, 'format' => 'full_html']);
                $transNode->field_tabelle->setValue(['value' => $tabelle, 'format' => 'full_html']);
                $transNode->field_zusatzliche_seiten->setValue(['value' => $zusaetliche_seiten, 'format' => 'full_html']);

                $transNode->save();
              }
            }

          }
        } else {
          error_log(DRUPAL_ROOT . '/import/' . $folder . '/' . $import_folder . '/' . $folder . '_' . $import_folder . '.xml not found');
        }
      }
    }

    return [
      '#type' => 'markup',
      '#markup' => '<ul><li>' . join('</li><li>', $messages) . '</li></ul>'
    ];
  }

  function _convert($string, $type = 'HTML') {
    $inXML   = ['<tiefgestellt>', '</tiefgestellt>', '<hochgestellt>', '</hochgestellt>'];
    $outXML  = ['[sub]', '[/sub]', '[sup]', '[/sup]'];
    $outHTML = ['<sub class="hks">', '</sub>', '<sup class="hks">', '</sup>'];

    if ($type == 'XML') {
      return str_replace($inXML, $outXML, $string);
    }
    else {
      return str_replace($outXML, $outHTML, $string);
    }
  }

  function _import_ueberschrift($ueberschrift) {
    $headline = preg_replace("/\p{Zl}/mu", " ", $ueberschrift);
    $headline = preg_replace("/\p{Zp}/mu", " ", $headline);
    $headline = preg_replace("//mu", "✓", $headline);
    $headline = preg_replace("/\(l\)/mu", "(●)", $headline);

    return html_entity_decode($this->_convert(htmlspecialchars($headline, ENT_QUOTES, 'UTF-8')))  ?: 'Produkt' . random_int(1, 99);
  }

  function _import_fliesstext_gesamt($fliesstext_gesamt) {
    $string = '';
    /* fliesstext_gesamt (gruppe headline, fliesstext) koennen haeufiger vorkommen */
    if ($fliesstext_gesamt) {
      foreach ($fliesstext_gesamt as $text) {
        $headline = preg_replace("/\p{Zl}/mu", " ", $text->fliesstext_ueberschrift);
        $headline = preg_replace("/\p{Zp}/mu", " ", $headline);
        $string .= '<strong>' . $this->_convert(htmlspecialchars($headline, ENT_QUOTES, 'UTF-8')) . '</strong>' . "\n";
        /* in einer gruppe fliesstext_gesamt koennen mehrere fliesstexte stehen  */
        if ($text->fliesstext) {
          foreach ($text->fliesstext as $fliesstext) {
            $fliesstext = preg_replace("/\p{Zl}/mu", " ", $fliesstext);
            $fliesstext = preg_replace("/\p{Zp}/mu", " ", $fliesstext);
            $string .= '<p>' . $this->_convert(htmlspecialchars($fliesstext, ENT_QUOTES, 'UTF-8')) . '</p>' . "\n";
          }
        }
      }
    }
    return $string;
  }

  function _import_fliesstext($fliesstext) {
    $string = '';
    /* fliesstexte koennen haeufiger vorkommen */
    if ($fliesstext) {
      foreach ($fliesstext as $ftext) {
        $text = preg_replace("/\p{Zl}/mu", " ", $ftext);
        $text = preg_replace("/\p{Zp}/mu", " ", $text);
        $string .= '<p>' . $this->_convert(htmlspecialchars($text, ENT_QUOTES, 'UTF-8')) . '</p>' . "\n";
        $ftcount++;
        /* wenn mehrere fliesstextbloecke existeren gibt es auch unterueberschriften */
        $uucount = 0;
        if ($prod->unterueberschrift) {
          foreach ($prod->unterueberschrift as $unterueberschrift) {
            if (++$uucount == $ftcount) {
              $string .= '<h3 class="prodlisthead">' . $this->_convert($unterueberschrift) . '</h3>' . "\n";
            }
          }
        }
      }
    }
  }

  function _processListen($listen, $language, $show_zusatz = false) {
    $string = '';

    /* hier koennen mehrere listen verarbeitet werden */
    for ($i = 0, $icounter = count($listen); $i < $icounter; $i++) {
      /* bei jeder neuen liste nach einer headline suchen */
      $listheadline = '';
      $is_zusatz = false;

      if ($listen[$i]->aufzaehlungs_ueberschrift) {
        /* filter out enter key */
        $item = preg_replace(
          "/
                  /mu",
          " <br /> ",
          $listen[$i]->aufzaehlungs_ueberschrift
        );

        // die Listen enthalten auch die Zusatzausstattungen
        // die Annahme ist dass die Zusatzausstattungen immer am Ende stehen!
        $headline = strip_tags($this->_convert($item));

        $listheadline = '<h3 class="prodlisthead">' . $this->_convert($item) . '</h3>' . "\n";

      }

      if ($listen[$i]->aufzaehlungs_ueberschrift) {

        $zusatz = [
          'deutsch' => 'Zusatzausstattung',
          'englisch' => 'Additional equipment',
          'spanisch' => 'Equipamiento opcional',
          'franzoesisch' => 'Options',
          'italienisch' => 'Dotazione aggiuntiva',
          'polnisch' => 'Wyposażenie dodatkowe',
          'russisch' => 'Дополнительное оборудование',
          'chinesisch' => '额外配置',
          'japanisch' => '付属装置',
        ];

        if (substr($headline, 0, strlen($zusatz[$language])) === $zusatz[$language]) {
          $is_zusatz = true;
        } else {
          $is_zusatz = false;
        }
      }

      if (!$show_zusatz && $is_zusatz) {
        continue;
      }
      if ($show_zusatz && !$is_zusatz) {
        continue;
      }

      if (strlen($listheadline)) {
        $string .= $listheadline;
      }
      /* und die liste starten */
      $string .= '<ul class="prodlist">' . "\n";
      /* verarbeitung der list items */
      for ($j = 0, $lacounter = count($listen[$i]->aufzaehlungspunkt); $j < $lacounter; $j++) {
        /* fancy indesign2html character converting */
        $item = trim($listen[$i]->aufzaehlungspunkt[$j]);
        $item = preg_replace("/\p{Zl}/mu", " ", $item); /*Line separator*/
        $item = preg_replace("/\p{Zp}/mu", " ", $item); /*Paragraph separator*/
        $item = preg_replace("/\p{Mc}/mu", " ", $item); /*Spacing mark*/
        $item = preg_replace("/\p{So}C/mu", "degreC", $item); /*save dregee*/
        $item = preg_replace("/\p{So}/mu", " ", $item); /*Other symbol*/
        $item = str_replace("degreC", " °C", $item); /*restore degree*/

        if (count($listen[$i]->aufzaehlungspunkt[$j]->aufzaehlungspunkt_ohne_fett) || count($listen[$i]->aufzaehlungspunkt[$j]->aufzaehlungspunkt_ohne)) {
          if (count($listen[$i]->aufzaehlungspunkt[$j]->aufzaehlungspunkt_ohne_fett)) {
            $item = preg_replace("/\p{Zl}/mu", " ", $listen[$i]->aufzaehlungspunkt[$j]->aufzaehlungspunkt_ohne_fett);
            $item = preg_replace("/\p{Zp}/mu", " ", $item);
            $item = preg_replace("/\p{So}/mu", " ", $item);
            $string .= '<ul class="ohne_fett">' . $this->_convert(htmlspecialchars($item, ENT_QUOTES, 'UTF-8'));
          }
          if (count($listen[$i]->aufzaehlungspunkt[$j]->aufzaehlungspunkt_ohne)) {
            $item = preg_replace("/\p{Zl}/mu", " ", $listen[$i]->aufzaehlungspunkt[$j]->aufzaehlungspunkt_ohne);
            $item = preg_replace("/\p{Zp}/mu", " ", $item);
            $item = preg_replace("/\p{So}/mu", " ", $item);
            $string .= '<i class="ohne">' . $this->_convert(htmlspecialchars($item, ENT_QUOTES, 'UTF-8'));
          }
        }
        else {
          $string .= '<li>' . $this->_convert(htmlspecialchars($item, ENT_QUOTES, 'UTF-8'));
        }
        /* check if there are sublists - falsche schreibweise vom kunden */
        if (count($listen[$i]->aufzaehlungspunkt[$j]->aufzaelungsstrich)) {
          $string .= '<ul class="prodsublist">' . "\n";
          for ($x = 0, $strichcounter = count($listen[$i]->aufzaehlungspunkt[$j]->aufzaelungsstrich); $x < $strichcounter; $x++) {
            $item = preg_replace("/\p{Zl}/mu", " ", $listen[$i]->aufzaehlungspunkt[$j]->aufzaelungsstrich[$x]);
            $item = preg_replace("/\p{Zp}/mu", " ", $item);
            $item = preg_replace("/\p{So}/mu", " ", $item);
            $string .= '<li>' . $this->_convert(htmlspecialchars($item, ENT_QUOTES, 'UTF-8')) . '</li>' . "\n";
          }
          $string .= '</ul>' . "\n";
        }
        /* check if there are sublists - korrekte schreibweise um sicher zu gehen */
        if (count($listen[$i]->aufzaehlungspunkt[$j]->aufzaehlungsstrich)) {
          $string .= '<ul class="prodsublist">' . "\n";
          for ($x = 0, $strichcounter = count($listen[$i]->aufzaehlungspunkt[$j]->aufzaehlungsstrich); $x < $strichcounter; $x++) {
            $item = preg_replace("/\p{Zl}/mu", " ", $listen[$i]->aufzaehlungspunkt[$j]->aufzaehlungsstrich[$x]);
            $item = preg_replace("/\p{Zl}/mu", " ", $listen[$i]->aufzaehlungspunkt[$j]->aufzaehlungsstrich[$x]);
            $item = preg_replace("/\p{Zp}/mu", " ", $item);
            $item = preg_replace("//mu", "✓", $item);
            $item = preg_replace("/\(l\)/mu", "(●)", $item);
            $item = preg_replace("/\p{So}C/mu", "degreC", $item); /*save dregee*/
            if ($language != 'cn') {
              $item = preg_replace("/\p{So}/mu", " ", $item);
            }
            $item = str_replace("degreC", " °C", $item); /*restore degree */
            $item = preg_replace("/^-/mu", "", $item);
            $string .= '<li>' . $this->_convert(htmlspecialchars($item, ENT_QUOTES, 'UTF-8')) . '</li>' . "\n";
          }
          $string .= '</ul>' . "\n";
        }
        /* EOF sublist and closing of list item */
        $string .= '</li>' . "\n";
      }
      $string .= '</ul>' . "\n";
    }
    return $string;
  }

  function _import_tabelle_gesamt($tabelle_gesamt) {
    $string = '';
    if ($tabelle_gesamt) {
      foreach ($tabelle_gesamt as $tbl) {
        $tableAttr = $tbl->tabelle->attributes('http://ns.adobe.com/AdobeInDesign/4.0/');
        $tblHead = $tbl->tabelle->tabellenueberschrift;
        $tblBody = $tbl->tabelle->tabellenwerte;
        $tblFooter = $tbl->tabelle->tabellenunterschrift;
        if (!$tbl->tabelle) {
          $tableAttr = $tbl->Tabelle->attributes('http://ns.adobe.com/AdobeInDesign/4.0/');
          $tblHead = $tbl->Tabelle->tabellenueberschrift;
          $tblBody = $tbl->Tabelle->tabellenwerte;
          $tblFooter = $tbl->Tabelle->tabellenunterschrift;
        }

        $string .= '<table class="prodtbl responsive">';
        if ($tblHead) {
          $string .= '<thead>' . "\n";
          $colCount = 1;
          foreach ($tblHead as $cell) {
            $cellAttr = $cell->attributes('http://ns.adobe.com/AdobeInDesign/4.0/');
            if ($colCount == 1) {
              $string .= '<tr>' . "\n";
              $string .= '<th class="model"';
            }
            else {
              $string .= '<th';
            }

            if ($cellAttr['crows'] > 1) {
              $string .= ' rowspan="' . $cellAttr['crows'] . '"';
            }
            if ($cellAttr['ccols'] > 1) {
              $string .= ' colspan="' . $cellAttr['ccols'] . '"';
              $colCount += $cellAttr['ccols'];
              $colCount--;
            }
            $cell_content = strlen($cell) ? $cell : '&nbsp;';
            $cell_content = str_replace("�", "<sup style='font-size: 8px'>1</sup>", $cell_content);
            $string .= '>' . $this->_convert($cell_content) . '</th>';
            if ($colCount == $tableAttr['tcols']) {
              $string .= '</tr>' . "\n";
              $colCount = 0;
            }
            $colCount++;
          }
          $string .= '</thead>' . "\n";
        }
        $string .= '<tbody>';

        if ($tblBody) {
          $colCount = 1;
          /* rowspan variables */
          $rowSpanArray = array();
          $processRowSpan = FALSE;
          /* eof rowspan variables */
          foreach ($tblBody as $cell) {
            $cellAttr = $cell->attributes('http://ns.adobe.com/AdobeInDesign/4.0/');
            if ($colCount == 1) {
              $string .= '<tr>' . "\n";
              $string .= '<td class="model prodtd' . $colCount . '"';
            }
            else {
              $string .= '<td class="prodtd' . $colCount . '"';
            }
            if ($cellAttr['crows'] > 1) {
              $string .= ' rowspan="' . $cellAttr['crows'] . '"';
              /* merke wieviele rows im rowspan enthalten sind - dient spaeter als zaehler */
              $rowSpanArray['rows'] = $cellAttr['crows'];
              /* merke wieviele rows im rowspan enthalten sind - dient dazu den ersten Aufruf zu ueberspringen */
              $rowSpanArray['reference'] = $rowSpanArray['rows'];
              /* merke wieviele cols (mindestens 1 FIX for http://www.nabertherm.de/produkte/details/de/glas_hochtemperaturoefen [Hochtemperatur-Hauben- und Elevatoröfen]) */
              if ($cellAttr['ccols'] > 1) {
                $rowSpanArray['cols'] = $cellAttr['ccols'];
              }
              else {
                $rowSpanArray['cols'] = 1;
              }
              /* merke die position (== colCount) */
              $rowSpanArray['pos'] = $colCount;
              $processRowSpan = TRUE;
            }
            /* wenn es einen rowspan gibt und wir in der richten column angekommen sind */
            if ($processRowSpan && $colCount == $rowSpanArray['pos']) {
              /* die row in der der rowspan definiert wurde soll uebersprungen werden */
              if ($rowSpanArray['reference'] > $rowSpanArray['rows']) {
                $colCount += $rowSpanArray['cols'];
              }
              $rowSpanArray['rows'] -= 1;
            }

            if ($cellAttr['ccols'] > 1) {
              $string .= ' colspan="' . $cellAttr['ccols'] . '"';
              $colCount += $cellAttr['ccols'];
              $colCount--;
            }
            if (utf8_encode($cell) == 'ï¼') {
              $cell_content = '&radic;';
            }
            elseif (utf8_encode($cell) == '') {
              $cell_content = '&radic;';
            }
            elseif (utf8_encode($cell) == 'l') {
              $cell_content = '&#9679;';
            }
            elseif ($cell == '¡') {
              $cell_content = ' • ';
            }
            else {
              $cell_content = strlen($cell) ? $cell : '&nbsp;';
            }

            $cell_content = str_replace('', '&radic;', $cell_content);
            $cell_content = str_replace('ï¼', '&radic;', $cell_content);

            $string .= '>' . $this->_convert($cell_content) . '</td>';

            if ($colCount == $tableAttr['tcols']) {
              $string .= '</tr>';
              $colCount = 0;
            }
            $colCount++;
          }
        }

        if ($tblFooter) {
          $colCount = 1;
          foreach ($tblFooter as $cell) {
            $cellAttr = $cell->attributes('http://ns.adobe.com/AdobeInDesign/4.0/');
            if ($colCount == 1) {
              $string .= '<tr class="tblfooter">';
            }
            $string .= '<td';
            if ($cellAttr['crows'] > 1) {
              $string .= ' rowspan="' . $cellAttr['crows'] . '"';
            }
            if ($cellAttr['ccols'] > 1) {
              $string .= ' colspan="' . $cellAttr['ccols'] . '"';
              $colCount += $cellAttr['ccols'];
              $colCount--;
            }
            $cell_content = strlen($cell) ? $cell : '&nbsp;';
            $cell_content = str_replace('ï¼', '&radic;', $cell_content);
            $cell_content = str_replace('', '&radic;', $cell_content);
            $cell_content = str_replace('¡', '•', $cell_content);
            $cell_content = preg_replace('/\sl\s/', ' &#9679; ', $cell_content);
            $cell_content = preg_replace("/\p{Zl}/mu", "<br />", $cell_content);
            $cell_content = preg_replace("/\p{Zp}/mu", "<br />", $cell_content);
            $cell_content = str_replace("�", "<sup style='font-size: 8px'>1</sup>", $cell_content);
            $string .= '>' . $this->_convert($cell_content) . '</td>';
            if ($colCount == $tableAttr['tcols']) {
              $string .= '</tr>';
              $colCount = 0;
            }
            $colCount++;
          }
        }
        if (substr($string, strlen($string) - 5) != '</tr>') {
          $string .= '</tr>';
        }
        $string .= '</tbody>' . "\n";
        $string .= '</table>' . "\n";
      }
    }
    return $string;
  }

  function _import_zusaetz_seiten($page) {
    $string = '';
    if ($page) {
      if ($page->ueberschrift) {
        $string .= '<strong>' . $page->ueberschrift . '</strong>';
      }
      if ($page->fliesstext) {
        $ftcount = 0;
        foreach ($page->fliesstext as $fliesstext) {
          $text = preg_replace("/\p{Zl}/mu", " ", $fliesstext);
          $text = preg_replace("/\p{Zp}/mu", " ", $text);
          $tmpStr = '<p>' . $this->_convert(htmlspecialchars($text, ENT_QUOTES, 'UTF-8')) . '</p>' . "\n";
          $ftcount++;
          /* wenn mehrere fliesstextbloecke existeren gibt es auch unterueberschriften */
          $uucount = 0;
          if ($page->unterueberschrift) {
            foreach ($page->unterueberschrift as $unterueberschrift) {
              if (++$uucount == $ftcount) {
                $tmpStr = '<h3 class="prodlisthead">' . $this->_convert($unterueberschrift) . '</h3>' . $tmpStr;
              }
            }
          }
          $string .= $tmpStr;
        }
      }
      /* fliesstext_gesamt (gruppe headline, fliesstext) koennen haeufiger vorkommen */
      if ($page->fliesstext_gesamt) {
        $ftcount = 0;
        foreach ($page->fliesstext_gesamt as $fliesstext_gesamt) {
          $headline = preg_replace("/\p{Zl}/mu", " ", $fliesstext_gesamt->fliesstext_ueberschrift);
          $headline = preg_replace("/\p{Zp}/mu", " ", $headline);
          $string .= '<strong>' . $this->_convert(htmlspecialchars($headline, ENT_QUOTES, 'UTF-8')) . '</strong>' . "\n";
          /* in einer gruppe fliesstext_gesamt koennen mehrere fliesstexte stehen  */
          if ($fliesstext_gesamt->fliesstext) {
            foreach ($fliesstext_gesamt->fliesstext as $fliesstext) {
              $fliesstext = preg_replace("/\p{Zl}/mu", " ", $fliesstext);
              $fliesstext = preg_replace("/\p{Zp}/mu", " ", $fliesstext);
              $string .= '<p>' . $this->_convert(htmlspecialchars($fliesstext, ENT_QUOTES, 'UTF-8')) . '</p>' . "\n";
            }
          }
        }
      }
      if ($page->aufzaehlung_gesamt) {
        $string .= $this->_processListen($page->aufzaehlung_gesamt);
      }
    }
  }

  function _processImages(&$images, $location) {

    $produkte_image = [];
    $de_images = $images->deutsch->produktbild_gesamt;
    $counter = 0;

    foreach ($de_images as $bild) {

      if ($bild->bild_oben) {
        $imgAttr = $bild->bild_oben->attributes();
      }
      elseif ($bild->bild_links) {
        $imgAttr = $bild->bild_links->attributes();
      }
      elseif ($bild->bild_inliste) {
        $imgAttr = $bild->bild_inliste->attributes();
      }
      else {
        return [];
      }

      $img_parts = pathinfo($imgAttr['href']);

      $image_name = str_replace('.' . $img_parts['extension'], '', $img_parts['filename']);
      $image_basename = $image_name;
      $image_path = '';

      if (file_exists($location . '/Large/' . urldecode($image_name) . '_fmt.jpg')) {
        $image_path = $location . '/Large/' . urldecode($image_name) . '_fmt.jpg';
        $image_name .= '_fmt.jpg';
      }
      elseif (file_exists($location . '/Large/' . urldecode($image_name) . '_fmt.jpeg')) {
        $image_path = $location . '/Large/' . urldecode($image_name) . '_fmt.jpeg';
        $image_name .= '_fmt.jpeg';
      }
      elseif (file_exists($location . '/Large/' . urldecode($image_name) . '_opt.jpg')) {
        $image_path = $location . '/Large/' . urldecode($image_name) . '_opt.jpg';
        $image_name .= '_opt.jpg';
      }
      elseif (file_exists($location . '/Large/' . urldecode($image_name) . '_opt.jpeg')) {
        $image_path = $location . '/Large/' . urldecode($image_name) . '_opt.jpeg';
        $image_name .= '_opt.jpeg';
      }
      else {
        $message = $location . '/Large/' . urldecode($image_name) . ' not found';
        \Drupal::logger('nabertherm_produkt_importer')->notice($message);
      }

      if(!strlen($image_path)) {
        continue;
      }
      $file_data = file_get_contents($image_path);
      $file = file_save_data($file_data, 'public://' . $image_name, 'EXISTS_REPLACE');

      $caption = '';
      $caption = preg_replace("/\p{Zl}/mu", " ", $bild->bildunterschrift);
      $caption = preg_replace("/\p{Zp}/mu", " ", $caption);
      $caption = $this->_convert(htmlspecialchars($caption, ENT_QUOTES, 'UTF-8'));
      $caption = strip_tags($caption);
      if (strlen($caption) > 255) {
        $caption = mb_substr($caption, 0, 200);
      }

      $media = Media::create([
        'bundle'           => 'image',
        'uid'              => \Drupal::currentUser()->id(),
        'field_media_image' => [
          'target_id' => $file->id(),
          'alt' => $image_basename,
        ],
        'field_media_caption' => [
          'value' => $caption,
          ],
      ]);
      $media->setName($image_basename)->setPublished(TRUE)->save();

      $translations = array(
        'test' => 'test',
        'en' => 'englisch',
        'es' => 'spanisch',
        'fr' => 'franzoesisch',
        'it' => 'italienisch',
        'pl' => 'polnisch',
        'ru' => 'russisch',
        'ja' => 'japanisch',
        'zh-hans' => 'chinesisch',
      );
      foreach ($translations as $key => $value) {
        if (!isset($images->$value)) {
          continue;
        }

        $tans_images = $images->$value->produktbild_gesamt;
        $trans_img = $tans_images[$counter];
        $caption = '';
        $caption = preg_replace("/\p{Zl}/mu", " ", $trans_img->bildunterschrift);
        $caption = preg_replace("/\p{Zp}/mu", " ", $caption);
        $caption = $this->_convert(htmlspecialchars($caption, ENT_QUOTES, 'UTF-8'));
        $caption = strip_tags($caption);
        if (strlen($caption) > 255) {
          $caption = mb_substr($caption, 0, 200);
        }

        $image_translation = $media->addTranslation($key, $media->toArray());
        $image_translation->field_media_caption = $caption;
        $image_translation->save();
      }

      $produkte_image[$media->id()] = $caption;
      ++$counter;
    }
    return $produkte_image;
  }

}
