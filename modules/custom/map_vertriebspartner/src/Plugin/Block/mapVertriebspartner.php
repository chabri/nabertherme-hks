<?php

/**
 * @file
 * Contains \Drupal\map_vertriebspartner\Plugin\Block\mapVertriebspartner.
 */

namespace Drupal\map_vertriebspartner\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\taxonomy\Entity\Term;      

/**
 * Provides a 'Next Previous' block.
 *
 * @Block(
 *   id = "map_vertriebspartner_block",
 *   admin_label = @Translation("Vertriebspartner Map"),
 *   category = @Translation("Blocks")
 * )
 */
class mapVertriebspartner extends BlockBase {
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * {@inheritdoc}
   */
  public function build() {
    
      $current_langcode =  \Drupal::languageManager()->getCurrentLanguage()->getId();
      $nids = \Drupal::entityQuery('node')->condition('type','vertriebspartner')->condition('langcode', $current_langcode)->execute();
      $continentsTids = \Drupal::entityQuery('taxonomy_term')->condition('vid', 'continents')->sort('weight')->execute();
  
      $nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);
      $termsContinents = Term::loadMultiple($continentsTids);
      
      return [
        '#label' => $this->configuration['label'],
        '#markup' => $this->configuration['map_textarea'],
        '#nodes' => $nodes,
        '#continents' => $termsContinents,
        '#attached' => array(
            'library' => array(
                'map_vertriebspartner/lib_vertriebspartner',
            ),
        ),
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $form['map_textarea'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text'),
      '#default_value' => isset($config['map_textarea']) ? $config['map_textarea'] : '',
      '#format' => 'full_html',
      '#base_type' => 'textarea',
    ];
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['map_textarea'] = $values['map_textarea']['value'];

  }
  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

}
