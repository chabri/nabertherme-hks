/**
 * @file
 * Javascript for Color Field.
 */

(function ($, Drupal) {
    'use strict';
    function unique(list) {
        var result = [];
        $.each(list, function(i, e) {
            if ($.inArray(e, result) == -1) result.push(e);
        });
        return result;
    }
    $(document).ready(function(){
        
        var container = $('.map_vertriebspartner_block'),
        items = container.find('.views-row'),
        countries = [],
        categories = [],
        select = container.find('select'),
        selectContinent = container.find('#continentselect'),
        selectCountry = container.find('#landselect'),
        selectCategorie = container.find('#kategorieelect');
        items.each(function(){
            var element = $(this),
            country_name = element.attr('data-country'),
            country_code = element.attr('data-countrycode'),
            continent = element.attr('data-continent'),
            categorieName = [],
            caregorieArray = element.attr('data-categorie').split('|');
            $.each(caregorieArray, function(i, e) {
                if(e === ''){
                    // we need it?
                }else{
                    categorieName.push('<option value="'+e+'" class="'+ continent +' '+ country_code + '">'+e.replace(/_/g, ' ')+'</option>');
                }
            });
            categories.push(categorieName);
            if(country_name === ''){
                // we need it?
            }else{
                countries.push('<option value="'+country_code+'" class="'+ continent +' '+ country_code + '">'+country_name+'</option>');
            }
            
        }); 
        
        function categorieSelect(){
            
        }
        
        
        selectCategorie.html('<option selected value="all">-- '+ Drupal.t('Kategorie') +' --</option>' + unique(categories));
        selectCountry.html('<option selected value="all" >-- '+ Drupal.t('Land') +' --</option>' + unique(countries));
        
        var allOptionsCountry = $('#landselect option');
        var allOptionsCategory = $('#kategorieelect option');
        selectContinent.change(function () {
            $('#landselect option:not(:first)').remove()
            $('#kategorieelect option:not(:first)').remove()
            selectCategorie.prop("disabled", true);
            var classContinent = $('#continentselect option:selected').prop('class');
            if($(this).val() === 'all'){
                var optsCountry = allOptionsCountry;
                var optsCategory = allOptionsCategory;
            }else{
                var optsCountry = allOptionsCountry.filter('.' + classContinent);
                var optsCategory = allOptionsCategory.filter('.' + classContinent);
            }
            if($(this).val() === 'all'){
                selectCountry.prop("disabled", true);
            }else{
                selectCountry.prop("disabled", false);
            }
            $.each(optsCountry, function (i, j) {
                $(j).appendTo('#landselect');
            });
            $.each(optsCategory, function (i, j) {
                $(j).appendTo('#kategorieelect');
            });
        });

        selectCountry.change(function () {
            $('#kategorieelect option:not(:first)').remove()
            var value = $(this).val();
            var classCountry = $('#landselect option:selected').prop('class');
            var optsCategory = allOptionsCategory.filter('.' + classCountry.split(' ')[1]);

            if(value === 'all'){
                selectCategorie.prop("disabled", true);
            }else{
                selectCategorie.prop("disabled", false);
            }

            $.each(optsCategory, function (i, j) {
                $(j).appendTo('#kategorieelect');
            });
            var seen = {};
            $('#kategorieelect option').each(function() {
                var txt = $(this).text();
                if (seen[txt])
                    $(this).remove();
                else
                    seen[txt] = true;
            });
        });
        
        
        var filterActive;

        function filterCategory(cat1, cat2, cat3) {
console.log(cat3);
            // reset results list
            $('.map_vertriebspartner_block .views-row').removeClass('active');

            // the filtering in action for all criteria
            var selector = ".map_vertriebspartner_block .views-row";
            if (cat1 !== 'all') {
                 selector = '[data-continent=' + cat1 + ']';
            }
            if (cat2 !== 'all') {
                selector = selector + '[data-countrycode=' + cat2 + ']';
            }
            if (cat3 !== 'all') {
                selector = selector + '[data-categorie*="' + cat3 + '"]';
            }
            console.log('selector' + selector);
            // show all results
            $(selector).addClass('active');
            if($(selector).length == 0){
                 $("html, body").animate({ scrollTop: $(".paragraph-breadcrumb").offset().top -100 }, 500)
                     .promise()
                     .then(function() {
                     $(".paragraph-breadcrumb").next().addClass('not-found');
                     setTimeout(function(){ $('.not-found').removeClass('not-found'); }, 3000);

                });
            }

            // reset active filter
            filterActive = cat1;
        }

        // start by showing all items
        $('.map_vertriebspartner_block .views-row').addClass('active');

        // call the filtering function when selects are changed
        select.change(function() {
            filterCategory(selectContinent.val(), selectCountry.val(), selectCategorie.val());

        });
        
    
        $('.svg').find("g.karte-kontinent").click(function(){
            var value = $(this).attr('id');
            $("html, body").animate({ scrollTop: $(".filters").offset().top -100 }, 500);
            selectContinent.val(value);
            selectContinent.trigger('change');
        });
    });

})(jQuery, Drupal);
