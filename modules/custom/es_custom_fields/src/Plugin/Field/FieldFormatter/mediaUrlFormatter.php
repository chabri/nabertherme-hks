<?php

namespace Drupal\es_custom_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\Core\Url;
use Drupal\Core\Language\LanguageInterface;
/**
 * Plugin implementation of the 'custom_media_url_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "custom_media_url_formatter",
 *   module = "es_custom_fields",
 *   label = @Translation("Media Document URL"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class mediaUrlFormatter extends FormatterBase {


  /**
   * {@inheritdoc}
   */
    public function viewElements(FieldItemListInterface $items, $langcode) {
        $elements = [];
        $languages = \Drupal::languageManager()->getLanguages();
        foreach ($items as $delta => $item) {

            // Get the media item.
            $media_id = $item->getValue()['target_id'];
            $media_item = Media::load($media_id);

            $fid = $media_item->field_media_document->target_id;
            $file = \Drupal\file\Entity\File::load($fid);
            $katalogSRC = file_create_url($file->getFileUri());        

//            $url = $file->url();

            $elements[$delta] = [
            '#type' => 'markup',
            '#markup' => $katalogSRC,
            ];

        }

        return $elements;
      }



}
