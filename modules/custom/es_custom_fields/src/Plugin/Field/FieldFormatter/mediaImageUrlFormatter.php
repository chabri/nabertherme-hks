<?php

namespace Drupal\es_custom_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Url;
/**
/**
 * Plugin implementation of the 'custom_media_image_url_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "custom_media_image_url_formatter",
 *   module = "es_custom_fields",
 *   label = @Translation("Media Image URL Formatter"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class mediaImageUrlFormatter extends FormatterBase {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;



  /**
   * {@inheritdoc}
   *
   * This has to be overridden because FileFormatterBase expects $item to be
   * of type \Drupal\file\Plugin\Field\FieldType\FileItem and calls
   * isDisplayed() which is not in FieldItemInterface.
   */
  protected function needsEntityLoad(EntityReferenceItem $item) {
    return !$item->hasNewEntity();
  }
  public static function defaultSettings() {
    return [
      'image_style' => '',
    ] + parent::defaultSettings();
  }
    public function settingsForm(array $form, FormStateInterface $form_state) {
        $settings = parent::settingsForm($form, $form_state);
        $image_styles = image_style_options(FALSE);
        $image_styles_hide = $image_styles;
        $image_styles_hide['hide'] = $this->t('Hide (do not display image)');

        
//        dump($settings['fancy_image_caption']);

        $element['image_style'] = [
          '#title' => $this->t('Image style'),
          '#type' => 'select',
          '#default_value' => $this->getSetting('image_style'),
          '#empty_option' => $this->t('None (original image)'),
          '#options' => $image_styles,
        ];
//          dump($element);
        return $element;
    }
    
    
/**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();

    $image_styles = image_style_options(FALSE);

    unset($image_styles['']);

    return $summary;
  }
  /**
   * {@inheritdoc}
   */
    public function viewElements(FieldItemListInterface $items, $langcode) {
        $elements = [];
        $settings = $this->getSettings();
        foreach ($items as $delta => $media) {

            $image_style_setting = $this->getSetting('image_style');
            $mediaImage = Media::load($media->getValue()['target_id']);
            $fid = $mediaImage->field_media_image->target_id;
            $file = File::load($fid);
            $image_uri = ImageStyle::load($image_style_setting)->buildUrl($file->getFileUri());       

//            $url = $file->url();

            $elements[$delta] = [
            '#type' => 'markup',
            '#markup' => $image_uri,
            ];

        }

        return $elements;
    }

}
