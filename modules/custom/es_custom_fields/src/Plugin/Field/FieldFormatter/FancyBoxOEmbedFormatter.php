<?php

namespace Drupal\es_custom_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Url;
/**
 * Plugin implementation of the 'fancybox_oembed' formatter.
 *
 * @internal
 *   This is an internal part of the oEmbed system and should only be used by
 *   oEmbed-related code in Drupal core.
 *
 * @FieldFormatter(
 *   id = "fancybox_oembed",
 *   label = @Translation("Video Fancybox"),
 *   field_types = {
 *     "link",
 *     "string",
 *     "string_long",
 *   },
 * )
 */
class FancyBoxOEmbedFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];


    foreach ($items as $delta => $item) {
        $main_property = $item->getFieldDefinition()->getFieldStorageDefinition()->getMainPropertyName();
        $value = $item->{$main_property};
        $media = $item->getParent()->getParent()->getEntity();
        $mediaVideo = Media::load($media->mid->getValue()['0']['value']);
        $url = '';
        if(!empty($media->field_media_video_file)){
            $media_field = $media->get('field_media_video_file')->first()->getValue();
            $file = File::load($media_field['target_id']);
            $url = file_create_url($file->getFileUri());
        }
        if(!empty($media->field_media_oembed_video)){
            $media_field = $media->get('field_media_oembed_video')->first()->getValue();
            $url = $media_field['value'];
        }
        $element[$delta] = [
            '#type' => 'markup',
            '#markup' => '<a class="video-name-popup" data-fancybox href="'.$url.'"><i class="icon-play"></i><span>'.$value.'</span></a>',
        ];
    }
    return $element;
  }



}
