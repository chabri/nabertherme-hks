<?php

namespace Drupal\es_custom_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'es_custom_padding_field' formatter.
 *
 * @FieldFormatter(
 *   id = "es_fields_padding_default",
 *   module = "es_custom_fields",
 *   label = @Translation("Padding"),
 *   field_types = {
 *     "es_fields_padding"
 *   }
 * )
 */
class PaddingFieldFormatter extends FormatterBase {


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
        $elements = array();
        foreach ($items as $delta => $item) {
               
            $top = !empty($item->paddingTop)    ? $item->paddingTop: '0';
            $right = !empty($item->paddingRight)  ? $item->paddingRight: '0';
            $bottom = !empty($item->paddingBottom) ? $item->paddingBottom: '0';
            $left = !empty($item->paddingLeft)   ? $item->paddingLeft: '0';
                
            $elements[$delta] = array(
                '#type' => 'markup',
                '#markup' => $markup = $top.'px ' . $right.'px ' . $bottom.'px ' . $left.'px',
            );
        }
      return $elements;
  }
  


}
