<?php

namespace Drupal\es_custom_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\Core\Url;
use Drupal\Core\Language\LanguageInterface;
/**
 * Plugin implementation of the 'custom_media_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "custom_media_formatter",
 *   module = "es_custom_fields",
 *   label = @Translation("Show all document Translations"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class mediaFormatter extends FormatterBase {


  /**
   * {@inheritdoc}
   */
    public function viewElements(FieldItemListInterface $items, $langcode) {
        $elements = [];
        $languages = \Drupal::languageManager()->getLanguages();
        $currentFile = "";
        foreach ($items as $delta => $item) {

          // Get the media item.
            $media_id = $item->getValue()['target_id'];
            $media_item = Media::load($media_id);
            $field = $media_item;
            $itemId = $item->getParent()->getName();
            $currentFile = file_create_url(\Drupal\file\Entity\File::load($field->field_media_document->target_id)->getFileUri());
            $html = '<div class="download-button-media">';
            $html .= '<ul>';
            $html .= '<li class="expanded"><a class="btn" href="#">'.t('Katalog download').'<i class="icon-download"></i></a>';
            $html .= '<ul class="list">';
             foreach($languages as $lang) {
                
                $katalog = \Drupal::service('entity.repository')->getTranslationFromContext($item->getEntity(), $lang->getId());
        

                if(!empty($katalog->get($itemId)->getValue()['0'])){
                     if($katalog->hasTranslation($lang->getId())){
                        $mid = $katalog->get($itemId)->getValue()['0']['target_id'];
                        $media = Media::load($mid);
                        $fid = $media->field_media_document->target_id;    
                        $file = \Drupal\file\Entity\File::load($fid);
                        $katalogSRC = file_create_url($file->getFileUri());
                        $html .= '<li><a href="'.$katalogSRC.'" download>'.$lang->getName().'<i class="icon-download"></i></a></li>';
                     }
                }
             }
            $html .= '</ul>';
            $html .= '</li>';
            $html .= '</ul>';
            $html .= '</div>';

            $elements[$delta] = [
            '#type' => 'markup',
            '#urlFile' => $currentFile,
            '#markup' => $html,
            ];

        }

        return $elements;
      }



}
