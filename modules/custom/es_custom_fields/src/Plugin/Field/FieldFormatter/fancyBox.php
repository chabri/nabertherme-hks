<?php

namespace Drupal\es_custom_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Url;
/**
 * Plugin implementation of the 'custom_fancybox_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "custom_fancybox_formatter",
 *   module = "es_custom_fields",
 *   label = @Translation("Fancy Box"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class fancyBox extends FormatterBase {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;



  /**
   * {@inheritdoc}
   *
   * This has to be overridden because FileFormatterBase expects $item to be
   * of type \Drupal\file\Plugin\Field\FieldType\FileItem and calls
   * isDisplayed() which is not in FieldItemInterface.
   */
  protected function needsEntityLoad(EntityReferenceItem $item) {
    return !$item->hasNewEntity();
  }
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_style' => '',
      'fancy_image_style' => '',
      'show_caption' => '',
    ] + parent::defaultSettings();
  }
    public function settingsForm(array $form, FormStateInterface $form_state) {
        $settings = parent::settingsForm($form, $form_state);
        $image_styles = image_style_options(FALSE);
        $image_styles_hide = $image_styles;
        $image_styles_hide['hide'] = $this->t('Hide (do not display image)');

        
//        dump($settings['fancy_image_caption']);

        $element['image_style'] = [
          '#title' => $this->t('Image style'),
          '#type' => 'select',
          '#default_value' => $this->getSetting('image_style'),
          '#empty_option' => $this->t('None (original image)'),
          '#options' => $image_styles,
        ];
        $element['fancy_image_style'] = [
          '#title' => $this->t('Image style for Fancy box'),
          '#type' => 'select',
          '#default_value' => $this->getSetting('fancy_image_style'),
          '#empty_option' => $this->t('None (original image)'),
          '#options' => $image_styles,
        ];
        $element['show_caption'] = [
          '#title' => $this->t('Image Caption'),
          '#description' => $this->t('Check this checkbox to display the Caption Image.'),
          '#default_value' => $this->getSetting('show_caption'),
          '#type' => 'checkbox',
          '#default_value' => $this->getSetting('show_caption'),
        ];
//          dump($element);
        return $element;
    }
    
    
/**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();

    $image_styles = image_style_options(FALSE);

    unset($image_styles['']);

    if (isset($image_styles[$this->getSetting('fancy_image_style')])) {
      $summary[] = $this->t('Fancy image style: @style', ['@style' => $image_styles[$this->getSetting('fancy_image_style')]]);
    }
    $summary[] = ($settings['show_caption'] == 1) ? $this->t('Display Caption : Yes') : $this->t('Display Caption : No');

    return $summary;
  }
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $image_link = "";
    $image_uri = "";
    $captionField = "";
    $caption = "";
    foreach ($items as $delta => $media) {
        $image_style_setting = $this->getSetting('image_style');
        $image_link_setting = $this->getSetting('fancy_image_style');
        $mediaImage = Media::load($media->getValue()['target_id']);
        $fid = $mediaImage->field_media_image->target_id;
        $file = File::load($fid);
        if(!empty($file)){
            $image_link = ImageStyle::load($image_link_setting)->buildUrl($file->getFileUri());
            $image_uri = ImageStyle::load($image_style_setting)->buildUrl($file->getFileUri());
        }
        if($mediaImage->hasTranslation($language)){
            if($mediaImage->getTranslation($language)->get('field_media_caption')->value){
                 $captionField = $mediaImage->getTranslation($language)->get('field_media_caption')->value;
            }
        }else{
            if($mediaImage->hasTranslation('en')){
                if($mediaImage->getTranslation('en')->get('field_media_caption')->value){
                    $captionField = $mediaImage->getTranslation('en')->get('field_media_caption')->value;
                }else{
                    $captionField = $mediaImage->getName();
                }
            }else{
                $captionField = $mediaImage->getName();
            }
        }

        $image_caption_settings = $this->getSetting('show_caption');
        if(!empty($image_caption_settings == '1')){
            $caption = '<figcaption>'.$mediaImage->getName().'</figcaption>';
        }

        $elements[$delta] = [
            '#type' => 'markup',
            '#title' => $captionField,
            '#imageFancy' => $image_link,
            '#image' => $image_uri,
            '#markup' => '<a href="'.$image_link.'" data-fancybox="gallery" class="item-image-gallery" title="'.$mediaImage->getName().'"><img alt="'.$mediaImage->getName().'" src="'.$image_uri.'" /><span><i class="icon-full-screen"></i></span>'.$caption.'</a>',
        ];

    }


    return $elements;
  }

}
