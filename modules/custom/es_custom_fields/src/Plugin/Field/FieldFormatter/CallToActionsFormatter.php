<?php

namespace Drupal\es_custom_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'es_custom_fields_cta' formatter.
 *
 * @FieldFormatter(
 *   id = "es_custom_fields_cta",
 *   module = "es_custom_fields",
 *   label = @Translation("Displays CTA list"),
 *   field_types = {
 *     "es_cta_fields"
 *   }
 * )
 */
class CallToActionsFormatter extends FormatterBase {
    
      public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
          $plugin_id,
          $plugin_definition,
          $configuration['field_definition'],
          $configuration['settings'],
          $configuration['label'],
          $configuration['view_mode'],
          $configuration['third_party_settings'],
        );
      }
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
      $elements = array();
        foreach ($items as $delta => $item) {
            $markup = $item->title;
            $markup .= $item->link_text;
            $markup .= $item->link_url;

            $elements[$delta] = array(
                '#theme' => 'es_custom_fields_cta_formatter',
                '#title' => $item->title,
                '#link_text' => $item->link_text,
                '#link_url' => $item->link_url,
            );
        }
      return $elements;
  }
  


}
