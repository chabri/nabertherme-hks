<?php

namespace Drupal\es_custom_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'es_custom_margin_field' formatter.
 *
 * @FieldFormatter(
 *   id = "es_fields_margin_default",
 *   module = "es_custom_fields",
 *   label = @Translation("Margin"),
 *   field_types = {
 *     "es_fields_margin"
 *   }
 * )
 */
class MarginFieldFormatter extends FormatterBase {


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
      $elements = array();
        foreach ($items as $delta => $item) {
            $top = !empty($item->marginTop)    ? $item->marginTop: '0';
            $right = !empty($item->marginRight)  ? $item->marginRight: '0';
            $bottom = !empty($item->marginBottom)   ? $item->marginBottom: '0';
            $left = !empty($item->marginLeft) ? $item->marginLeft: '0';
            $elements[$delta] = array(
                '#type' => 'markup',
                '#markup' => $markup = $top.'px ' . $right.'px ' . $bottom.'px ' . $left.'px',
            );
        }
      return $elements;
  }
  


}
