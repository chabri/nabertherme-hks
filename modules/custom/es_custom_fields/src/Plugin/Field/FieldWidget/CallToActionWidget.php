<?php

namespace Drupal\es_custom_fields\Plugin\Field\FieldWidget;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\LinkItemInterface;
/**
 * Plugin implementation of the 'es_custom_fields' widget.
 *
 * @FieldWidget(
 *   id = "es_custom_fields_cta",
 *   module = "es_custom_fields",
 *   label = @Translation("CTA Formated"),
 *   field_types = {
 *     "es_cta_fields"
 *   }
 * )
 */
class CallToActionWidget extends WidgetBase {
      protected static function getUserEnteredStringAsUri($string) {
    // By default, assume the entered string is a URI.
//          dump($string);
    $uri = trim($string);
          $node = \Drupal\node\Entity\Node::load($string);

        
      $uri = '""'.$node->getTitle() .' ('.$string.')""';
    

    return $uri;
  }

  /**
   * {@inheritdoc}
   */
public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $item = $items[$delta];

    $element['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => isset($items[$delta]->title) ? $items[$delta]->title : '',
      '#size' => 60,
    );
    $element['link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Link Text'),
      '#default_value' => isset($items[$delta]->link_text) ? $items[$delta]->link_text : '',
      '#size' => 60,
    );
//    dump($item->link_url);
    $element['link_url'] = array(
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => $this->t('URL'),
      '#empty_value' => '',
      '#default_value' => (!$item->isEmpty()) ? static::getUserEnteredStringAsUri($item->link_url) : NULL,
      '#link_type' => LinkItemInterface::LINK_GENERIC,
    );
      $element['link_url']['#attributes']['data-autocomplete-first-character-blacklist'] = '/#?';
      $element['link_url']['#process_default_value'] = FALSE;

   
      $element += array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('container')),
      );
    

    return $element;
  }


  /**
   * Validate the color text field.
   */
  public function validate($element, FormStateInterface $form_state) {

  }

}
