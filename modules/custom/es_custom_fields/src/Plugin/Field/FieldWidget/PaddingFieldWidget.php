<?php

namespace Drupal\es_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'es_custom_padding_field' widget.
 *
 * @FieldWidget(
 *   id = "es_fields_padding_default",
 *   module = "es_custom_fields",
 *   label = @Translation("Padding Formated"),
 *   field_types = {
 *     "es_fields_padding"
 *   }
 * )
 */
class PaddingFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $element['paddingTop'] = array(
      '#type' => 'number',
      '#title' => t('padding Top'),
      '#default_value' => isset($items[$delta]->paddingTop) ? $items[$delta]->paddingTop : '',
      '#size' => 10,
    );
    $element['paddingRight'] = array(
      '#type' => 'number',
      '#title' => t('padding Right'),
      '#default_value' => isset($items[$delta]->paddingRight) ? $items[$delta]->paddingRight : '',
      '#size' => 10,
    );
    $element['paddingBottom'] = array(
      '#type' => 'number',
      '#title' => t('padding Bottom'),
      '#default_value' => isset($items[$delta]->paddingBottom) ? $items[$delta]->paddingBottom : '',
      '#size' => 10,
    );
    $element['paddingLeft'] = array(
      '#type' => 'number',
      '#title' => t('padding Left'),
      '#default_value' => isset($items[$delta]->paddingLeft) ? $items[$delta]->paddingLeft : '',
      '#size' => 10,
    );

    // If cardinality is 1, ensure a label is output for the field by wrapping
    // it in a details element.
    
      $element += array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('container-inline')),
      );
    

    return $element;
  }


  /**
   * Validate the color text field.
   */
  public function validate($element, FormStateInterface $form_state) {

  }

}
