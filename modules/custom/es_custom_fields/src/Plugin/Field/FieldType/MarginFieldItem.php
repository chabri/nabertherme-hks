<?php

namespace Drupal\es_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'es_custom_fields' field type.
 *
 * @FieldType(
 *   id = "es_fields_margin",
 *   module = "es_custom_fields",
 *   label = @Translation("Margin"),
 *   description = @Translation("Margin"),
 *   default_widget = "es_fields_margin_default",
 *   default_formatter = "es_fields_margin_default"
 * )
 */
class MarginFieldItem extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'marginTop' => array(
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
        'marginRight' => array(
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
        'marginBottom' => array(
          'type' => 'text',
          'not null' => FALSE,
          'size' => 'tiny',
        ),
        'marginLeft' => array(
          'type' => 'text',
          'not null' => FALSE,
          'size' => 'tiny',
        ),
      ),
    );
  }

  public function isEmpty() {
    $value1 = $this->get('marginTop')->getValue();
    $value2 = $this->get('marginRight')->getValue();
    $value3 = $this->get('marginBottom')->getValue();
    $value4 = $this->get('marginLeft')->getValue();
    return empty($value1) && empty($value2) && empty($value3) && empty($value4);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Add our properties.
    $properties['marginTop'] = DataDefinition::create('string')->setLabel(t('Margin Top'));
    $properties['marginRight'] = DataDefinition::create('string')->setLabel(t('Margin Bottom'));
    $properties['marginBottom'] = DataDefinition::create('string')->setLabel(t('Margin Left'));
    $properties['marginLeft'] = DataDefinition::create('string')->setLabel(t('Margin Bottom'));

    return $properties;
  }

}
