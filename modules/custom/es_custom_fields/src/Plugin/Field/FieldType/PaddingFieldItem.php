<?php

namespace Drupal\es_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'es_custom_fields' field type.
 *
 * @FieldType(
 *   id = "es_fields_padding",
 *   module = "es_custom_fields",
 *   label = @Translation("Padding"),
 *   description = @Translation("Padding"),
 *   default_widget = "es_fields_padding_default",
 *   default_formatter = "es_fields_padding_default"
 * )
 */
class PaddingFieldItem extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'paddingTop' => array(
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
        'paddingRight' => array(
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
        'paddingBottom' => array(
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
        'paddingLeft' => array(
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
      ),
    );
  }

  public function isEmpty() {
    $value1 = $this->get('paddingTop')->getValue();
    $value2 = $this->get('paddingRight')->getValue();
    $value3 = $this->get('paddingBottom')->getValue();
    $value4 = $this->get('paddingLeft')->getValue();
    return empty($value1) && empty($value2) && empty($value3) && empty($value4);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Add our properties.
    $properties['paddingTop'] = DataDefinition::create('string')->setLabel(t('padding Top'));
    $properties['paddingRight'] = DataDefinition::create('string')->setLabel(t('padding Bottom'));
    $properties['paddingBottom'] = DataDefinition::create('string')->setLabel(t('padding Left'));
    $properties['paddingLeft'] = DataDefinition::create('string')->setLabel(t('padding Bottom'));

    return $properties;
  }

}
