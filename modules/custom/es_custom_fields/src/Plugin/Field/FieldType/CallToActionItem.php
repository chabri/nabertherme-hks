<?php

namespace Drupal\es_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Url;


/**
 * Plugin implementation of the 'es_custom_fields' field type.
 *
 * @FieldType(
 *   id = "es_cta_fields",
 *   label = @Translation("Call to Actions"),
 *   module = "es_custom_fields",
 *   description = @Translation("Call to actions."),
 *   default_widget = "es_custom_fields_cta",
 *   default_formatter = "es_custom_fields_cta"
 * )
 */
class CallToActionItem extends FieldItemBase  {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'title' => array(
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
        'link_text' => array(
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
        'link_url' => array(
          'description' => 'The URI of the link.',
          'type' => 'varchar',
          'length' => 2048,
        ),
      ),
    );
  }

  public function isEmpty() {

    $value1 = $this->get('title')->getValue();
    $value2 = $this->get('link_text')->getValue();
    $value3 = $this->get('link_url')->getValue();
    return $value1 === NULL || $value1 === '' && $value2 === NULL || $value2 === '' && $value3 === NULL || $value3 === '';
  }
    
  public function getUrl() {
    return Url::fromUri($this->link_url, (array) $this->options);
  }
  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Add our properties.
    $properties['title'] = DataDefinition::create('string')
      ->setLabel(t('Title'));

    $properties['link_text'] = DataDefinition::create('string')
      ->setLabel(t('Link Text'));

    $properties['link_url'] = DataDefinition::create('string')
      ->setLabel(t('Link'));


    return $properties;
  }

}
