<?php

namespace Drupal\nabertherm_filter\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Filters HTML responses to improve typography on some measure units.
 */
class UnitFilter implements EventSubscriberInterface {

  /**
   * The measure units to replace.
   *
   * Uses a narrow no-break space as separator between value and unit as
   * suggested by the IEEE Standards.
   *
   * @var array
   *
   * @see {@link https://en.wikipedia.org/wiki/Space_(punctuation)#Unit_symbols_and_numbers Unit symbols and numbers}
   */
  protected static $units = [
    '/([\d.,]+)\s*(?:°|º)(C|F)/' => '<data value="\1">\1 °\2</data>',
  ];

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => [['onResponse']],
    ];
  }

  /**
   * Transform the HTML markup.
   *
   * @param string $markup
   *   The HTML markup to transform.
   *
   * @return string
   *   The processed HTML markup.
   */
  public function transform($markup) {
    return preg_replace(array_keys(static::$units), array_values(static::$units), $markup);
  }

  /**
   * Add the 'with-hero'/'no-hero' class to the page component.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The response event.
   */
  public function onResponse(FilterResponseEvent $event) {
    $response = $event->getResponse();

    if (stripos($response->headers->get('Content-Type'), 'text/html') === FALSE) {
      return;
    }

    $content = $response->getContent();

    if ($content === FALSE) {
      return;
    }

    $response->setContent($this->transform($content));
  }

}
