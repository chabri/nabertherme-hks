<?php

/**
 * @file
 * Contains \Drupal\download_category_block\Plugin\Block\DownloadsBlock.
 */

namespace Drupal\download_category_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Language\LanguageInterface;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\Core\Form\FormStateInterface;
/**
 * Provides a 'downloaded categories with mith languages' block.
 *
 * @Block(
 *   id = "download_category_block_lang",
 *   admin_label = @Translation("Download Block"),
 *   category = @Translation("Blocks")
 * )
 */
class DownloadsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
        $language =  \Drupal::languageManager()->getCurrentLanguage()->getId();
        $termList = array();
        $languages = \Drupal::languageManager()->getLanguages();
        $langcodesList = array();
        $list = [];
        $terms = $this->configuration['category_select'];
//        $continentsTids = \Drupal::entityQuery('taxonomy_term')->condition('vid', 'continents')->sort('weight')->execute();
      
        foreach($terms as $term) {
            $loadTerm = \Drupal\taxonomy\Entity\Term::load($term);
            if($loadTerm->hasTranslation($language)){
                $translated_term = \Drupal::service('entity.repository')->getTranslationFromContext($loadTerm, $language);
                    foreach($languages as $lang) {
                        $list[]['language'] = $lang->getName();
                    }
                $tid = $loadTerm->id();
                $termList[$tid]['name'] = $translated_term->getName();
                $termList[$tid]['term'] = $loadTerm;
            }
        }

        $links = array();
        $html= ''; 
        foreach($termList as $tid => $item) {
            if(!empty($item['term']->get('field_katalog_file')->getValue()['0'])){
                $html = '<div class="download-list">';
                $html .= '<h4><div class="icon-pdf"><i class="icon-file-pdf"></i></div>'.$item['name'].'</h4>';
                $html .= '<ul>';
                $html .= '<li class="expanded"><a class="btn btn-theme">'.t('Sprache auswählen').'<i class="icon-down-open-mini"></i></a>';
                $html .= '<ul class="list">';
                foreach($languages as $lang) {
                    $term = $item['term'];
                    $term = \Drupal::service('entity.repository')->getTranslationFromContext($term, $lang->getId());
                    if(!empty($term->get('field_katalog_file')->getValue()['0'])){
                        $mid = $term->get('field_katalog_file')->getValue()['0']['target_id'];
                        $media = Media::load($mid);
                        $fid = $media->field_media_document->target_id;    
                        $file = \Drupal\file\Entity\File::load($fid);
                        $katalogSRC = file_create_url($file->getFileUri());
                        $html .= '<li><a href="'.$katalogSRC.'" download>'.$lang->getName().'<i class="icon-download"></i></a></li>';
                    }
                }

                $html .= '</ul>';
                $html .= '</li>';
                $html .= '</ul>';
                $html .= '</div>';
            }
            $links[]['#markup'] = $html;
        }
    return $links;
    }
/*
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $vid = 'produktkategorie';
    $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    $options =  [];
    foreach($terms as $term) {
           $options[$term->tid] = $term->name;
    }
//   dump($options);
    $form['category_select'] = array(
        '#type' => 'select',
        '#options' => $options,        
        '#title' => t('lower Measurement type'),
        '#size' => 10,
        '#multiple' => true,
        '#required' => FALSE,
        '#default_value' => isset($config['category_select']) ? $config['category_select'] : '',
        
    );

    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['category_select'] = $values['category_select'];

  }
  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }
    
}